package com.example.drawer.router;

public class Route {

    private static Route instance;
    private IRoute view;

    private Route(){

    }

    public static synchronized Route getInstance(){
        if (instance == null){
            return instance = new Route();
        }else {
            return instance;
        }
    }

    public void transitionFrAdd(){

    }

    public void startView(IRoute view){this.view = view;

    }

    public void stopView(){ if(view!=null) view=null;

    }
}
