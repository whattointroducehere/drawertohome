package com.example.drawer.presentational.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.drawer.R;
import com.example.drawer.databinding.ActivityMainBinding;
import com.example.drawer.presentational.base.BaseActivity;
import com.example.drawer.presentational.base.BasePresenter;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements PresenterContract.View {

    private PresenterContract.Presenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        getBinding().setEvent(presenter);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);

    }

    @Override
    protected void onDestroyView() {
        presenter.stopView();

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void openDrawer() {
        getBinding().drawerLayout.openDrawer();

    }

    @Override
    public void closeDrawer() {
        getBinding().drawerLayout.closeDrawers();

    }
}
