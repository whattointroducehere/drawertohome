package com.example.drawer.presentational.activity;

import com.example.drawer.presentational.base.BasePresenter;

public interface PresenterContract {

    interface View{
        void openDrawer();

        void closeDrawer();

    }

    interface Presenter extends BasePresenter<View> {

        void init();

        void openDrawer();

        void closeDrawer();


    }
}
