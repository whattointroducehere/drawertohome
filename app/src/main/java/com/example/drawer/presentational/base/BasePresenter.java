package com.example.drawer.presentational.base;

public interface BasePresenter<V> {
    void startView(V view);

    void stopView();
}
