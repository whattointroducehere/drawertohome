package com.example.drawer.presentational.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity <Binding extends ViewDataBinding> extends AppCompatActivity {

    private Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,getLayoutRes());
        initView();
    }





    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract void initView();

    protected abstract void onStartView();

    protected abstract void onDestroyView();

    protected Binding getBinding(){return binding;}

    protected abstract BasePresenter getPresenter();

    @Override
    protected void onStart() {
        super.onStart();
        onStartView();
    }



    @Override
    protected void onDestroy() {
        if(getPresenter()!=null){
            getPresenter().stopView();
            onDestroyView();
        }
        super.onDestroy();

    }
}
