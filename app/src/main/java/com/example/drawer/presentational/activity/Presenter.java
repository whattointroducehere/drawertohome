package com.example.drawer.presentational.activity;

public class Presenter implements PresenterContract.Presenter {

    private PresenterContract.View view;

    @Override
    public void startView(PresenterContract.View view) {

    }

    @Override
    public void stopView() {

    }

    @Override
    public void init() {

    }

    @Override
    public void openDrawer() {
        view.openDrawer();

    }

    @Override
    public void closeDrawer() {
        view.closeDrawer();
    }
}
