package com.example.drawer.app;

import android.app.Application;

import com.example.drawer.BuildConfig;

import timber.log.Timber;

public class App extends Application {

    public static App app;


    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        }
    }
}
